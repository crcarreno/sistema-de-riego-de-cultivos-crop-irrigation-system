
from flask import Flask, render_template, request
import logging
import sensors_rasp as sensors #Uso en Raspberry
#import sensors #Uso en PC
import model

app = Flask(__name__, template_folder='templates')

logger = logging.getLogger(__name__)

THREAD = sensors.Sensores()
THREAD.start()

THREAD_REPORT = sensors.Reportes()
THREAD_REPORT.start()


@app.route('/index', methods=['GET', 'POST'])
def index():

    data = model.ModelRiego()

    if request.method == 'POST':

        if request.form.get('active_riego_auto') == 'on':
            data.update_params('active_riego_auto', 'on')
            THREAD.set_auto_on()
        else:
            data.update_params('active_riego_auto', 'off')
            THREAD.set_auto_off()

        if request.form.get('active_bomba_agua') == 'on':
            data.update_params('active_bomba_agua', 'on')
            THREAD.set_pump_on()
        else:
            data.update_params('active_bomba_agua', 'off')
            THREAD.set_pump_off()

        if request.form.get('active_selenoide_izq') == 'on':
            data.update_params('active_selenoide_izq', 'on')
            THREAD.set_selenoide_left_on()
        else:
            data.update_params('active_selenoide_izq', 'off')
            THREAD.set_selenoide_left_off()

        if request.form.get('active_selenoide_der') == 'on':
            data.update_params('active_selenoide_der', 'on')
            THREAD.set_selenoide_right_on()
        else:
            data.update_params('active_selenoide_der', 'off')
            THREAD.set_selenoide_right_off()

        if request.form.get('active_grabar_est') == 'on':
            data.update_params('active_grabar_est', 'on')
            THREAD_REPORT.set_save_report_start()
        else:
            data.update_params('active_grabar_est', 'off')
            THREAD_REPORT.set_save_report_stop()

        if request.form.get('active_mostrar_est') == 'on':
            data.update_params('active_mostrar_est', 'on')
        else:
            data.update_params('active_mostrar_est', 'off')

    chart_fechas = []
    char_hum_amb = []
    char_tem_amb = []
    char_hum_sue = []

    for d in data.read_chart():
        chart_fechas.append(d[0])
        char_hum_amb.append(d[1])
        char_tem_amb.append(d[2])
        char_hum_sue.append(d[3])

    return render_template('index.html', data_params=data.read_params(),
                                        sensors_data=data.read_sensors(),
                                        chart_fechas=chart_fechas,
                                        char_hum_amb=char_hum_amb,
                                        char_tem_amb =char_tem_amb,
                                        char_hum_sue =char_hum_sue
                                        )


@app.route('/report', methods=['GET', 'POST'])
def report():
    data = model.ModelRiego()
    report_data = data.read_report()

    return render_template('reporte.html', report_data=report_data)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
