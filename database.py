# -*- coding: utf-8 -*-

import logging
import sqlite3

from sqlite3 import Error

logger = logging.getLogger(__name__)


class ConnectionDB:

    def create_connection(self):
        try:
            #conn = sqlite3.connect('data/db_riego')
            conn = sqlite3.connect('/home/pi/Desktop/Proyecto/riego/data/db_riego')

            logging.warning(sqlite3.version)

            return conn

        except Error as e:
            logging.warning(e.message)
        #finally:
        #    conn.close()
