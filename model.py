# -*- coding: utf-8 -*-

import database
import logging

data_model = database.ConnectionDB()


class ModelRiego:

    def read_params(self):

        conn = data_model.create_connection()
        cursor = conn.cursor()
        cursor.execute("select value, param from parametros")
        data = cursor.fetchall()

        cursor.close()
        conn.close()

        return data

    def read_report(self):

        conn = data_model.create_connection()
        cursor = conn.cursor()
        cursor.execute('''select id, hum_ambiente, tem_ambiente, hum_suelo, round(num_cultivo, 9), fecha_cre, hum_suelo2 from report
                        order by id DESC ;''')
        data = cursor.fetchall()

        cursor.close()
        conn.close()

        return data

    def read_chart(self):

        conn = data_model.create_connection()
        cursor = conn.cursor()
        cursor.execute('''select fecha_cre, hum_ambiente, tem_ambiente, hum_suelo from report order by id DESC limit 10;''')
        data = cursor.fetchall()

        cursor.close()
        conn.close()

        return data

    def read_sensors(self):

        conn = data_model.create_connection()
        cursor = conn.cursor()
        cursor.execute('''select id, hum_ambiente, tem_ambiente, hum_suelo, round(num_cultivo, 9), fecha_cre, hum_suelo2 from report
                        order by id DESC limit 1;''')
        data = cursor.fetchall()

        cursor.close()
        conn.close()

        return data

    def create_data_report(self, **kw):

        conn = data_model.create_connection()
        cursor = conn.cursor()

        row = (kw['hum_ambiente'],  kw['tem_ambiente'], kw['hum_suelo'], kw['num_cultivo'], kw['hum_suelo2'])

        sql = '''insert into report (hum_ambiente, tem_ambiente, hum_suelo, num_cultivo, fecha_cre, hum_suelo2)
                        values (?,?,?,?,CURRENT_TIMESTAMP, ?)'''

        cursor.execute(sql, row)

        conn.commit()
        cursor.close()
        conn.close()

    def update_params(self, param, value):

        if id:
            conn = data_model.create_connection()
            cursor = conn.cursor()

            sql = '''   update parametros
                        set value = "{}"
                        where param = "{}";'''.format(str(value), param)

            cursor.execute(sql)

            conn.commit()
            cursor.close()
            conn.close()