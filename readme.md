# **Sistema de riego automático de cultivos - Automatic crop irrigation system**

Proyecto para automatización de riego de cultivos utilizando raspberry y sensores.

El proyecto configura válvulas selenoides para retener y desviar el agua, así cómo una bomba de agua de 12V, que tienen como fin, propulsar el agua por las tuberías. Se programa una interfaz gráfica web muy agradable basada en bootstrap, la programación es realizada en Python, utilizando la librería de Flask para el entorno web, se configuran hilos de control, para realizar tareas automáticas de riego y para grabar los valores de los sensores. Estos valores se presentan en forma de reportes en la misma interfaz web.

Se configuran los ssensores para detectar las condiciones del suelo y el ambiente, con la finalidad de poder regar con agua los cultivos de manera automática.


**ENGLISH**

Project for automation of crop irrigation using raspberry and sensors.

The project configures selenoid valves to retain and divert water, as well as a 12V water pump, whose purpose is to propel water through the pipes. A very nice bootstrap-based web graphical interface is programmed, programming is done in Python, using the Flask library for the web environment, control threads are configured, to perform automatic irrigation tasks and to record the sensor values. These values ​​are presented in the form of reports in the same web interface.

The sensors are configured to detect soil and environmental conditions, in order to be able to water the crops automatically.