from gpiozero import DigitalInputDevice, OutputDevice
import time
#import Adafruit_DHT
import threading
import logging
import model
import random


class Reportes(threading.Thread):

    KILL_Report = False
    SAVE_REPORT = False

    def run(self):
        data = model.ModelRiego()
        while True:
            # SENSOR DE HUMEDAD Y TEMPERATURA
            #humidity, temperature = Adafruit_DHT.read_retry(11, GPIO_21)
            #print('Temp={0:0.1f}*  Humidity={1:0.1f}%'.format(temperature, humidity))

            if self.SAVE_REPORT:
                data.create_data_report(
                    hum_ambiente=random.uniform(42, 46),
                    tem_ambiente=random.uniform(24, 27),
                    hum_suelo=random.uniform(82, 85),
                    num_cultivo=random.uniform(1, 2),
                )
                print('Guardando registro de sensores')
            time.sleep(10)
        return

    def kill(self):
        self.KILL_Report = True

    def set_save_report_start(self):
        self.SAVE_REPORT = True

    def set_save_report_stop(self):
        self.SAVE_REPORT = False


class Sensores(threading.Thread):

    KILL = False
    SET_PUMB = False


    def run(self):
        count = 0
        while True:
            if self.KILL:
                print('KILL activado')
                break
            else:
                count += 1
                print(str(count) + ' - ' + str(self.SET_PUMB))
                time.sleep(1)
        return

    def kill(self):
        self.KILL = True

    def set_pump_on(self):
        self.SET_PUMB = True

    def set_pump_off(self):
        self.SET_PUMB = False
    '''
    # RELAY - SELENOIDES Y BOMBA DE AGUA
    GPIO_21 = 21
    GPIO_17 = DigitalInputDevice(17)
    GPIO_04 = DigitalInputDevice(4)

    RELAY_PIN_PUMB = 24
    RELAY_PIN_LEFT = 22
    RELAY_PIN_RIGHT = 23

    selenoide_left = OutputDevice(RELAY_PIN_LEFT, active_high=False, initial_value=False)
    selenoide_right = OutputDevice(RELAY_PIN_RIGHT, active_high=False, initial_value=False)
    pumb = OutputDevice(RELAY_PIN_PUMB, active_high=False, initial_value=False)
    '''
    def set_pump(self, status):
        if status:
            print("Selenoide PUMB: ON")
            self.pumb.on()
        else:
            print("Selenoide PUMB: OFF")
            self.pumb.off()

    def set_selenoide_right(self, status):
        if status:
            print("Selenoide RIGHT: ON")
            self.selenoide_right.on()
        else:
            print("Selenoide RIGHT: OFF")
            self.selenoide_right.off()

    def set_selenoide_left(self, status):
        if status:
            print("Selenoide LEFT: ON")
            self.selenoide_left.on()
        else:
            print("Selenoide LEFT: OFF")
            self.selenoide_left.off()


    '''   # SENSOR DE HUMEDAD Y TEMPERATURA
        # humidity, temperature = Adafruit_DHT.read_retry(11, GPIO_21)
        # print('Temp={0:0.1f}*  Humidity={1:0.1f}%'.format(temperature, humidity))

        # SENSOR DE SUELO
        if GPIO_04.value or GPIO_17.value:

            if GPIO_04.value and GPIO_17.value:
                set_selenoide_right(True)
                set_selenoide_left(True)
                print('GPIO_04: ' + str(GPIO_04.value) + ' - ' + 'GPIO_17: ' + str(GPIO_17.value))

            elif GPIO_04.value:
                set_selenoide_right(True)
                set_selenoide_left(False)
                print('GPIO_04: ' + str(GPIO_04.value) + ' - ' + 'GPIO_17: ' + str(GPIO_17.value))

            elif GPIO_17.value:
                set_selenoide_right(False)
                set_selenoide_left(True)
                print('GPIO_04: ' + str(GPIO_04.value) + ' - ' + 'GPIO_17: ' + str(GPIO_17.value))
            set_pump(True)
        else:
            set_selenoide_right(False)
            set_selenoide_left(False)
            set_pump(False)
            print('GPIO_04: ' + str(GPIO_04.value) + ' - ' + 'GPIO_17: ' + str(GPIO_17.value))

        time.sleep(3)
        '''