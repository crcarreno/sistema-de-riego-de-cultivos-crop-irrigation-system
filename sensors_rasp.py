from gpiozero import DigitalInputDevice, OutputDevice
import time
import Adafruit_DHT
import threading
import logging
import model
import random

GPIO_17 = DigitalInputDevice(17)  # SENSOR TEMP SUELO
GPIO_04 = DigitalInputDevice(4)  # SENSOR TEMP SUELO


class Reportes(threading.Thread):

    KILL_Report = False
    SAVE_REPORT = False
    GPIO_21 = 21

    def run(self):
        data = model.ModelRiego()
        while True:
            # SENSOR DE HUMEDAD Y TEMPERATURA
            humidity, temperature = Adafruit_DHT.read_retry(11, self.GPIO_21)
            #print('Temp={0:0.1f}*  Humidity={1:0.1f}%'.format(temperature, humidity))

            if self.SAVE_REPORT:
                hum_suelo = 0
                hum_suelo2 = 0

                if not GPIO_17.value:
                    hum_suelo = random.uniform(83, 85)

                if not GPIO_04.value:
                    hum_suelo2 = random.uniform(78, 83)

                data.create_data_report(
                    hum_ambiente=humidity,
                    tem_ambiente=temperature,
                    hum_suelo=hum_suelo,
                    hum_suelo2=hum_suelo2,
                    num_cultivo=random.uniform(1, 2),
                )
                print('Guardando registro de sensores')
            time.sleep(10)
        return

    def kill(self):
        self.KILL_Report = True

    def set_save_report_start(self):
        self.SAVE_REPORT = True

    def set_save_report_stop(self):
        self.SAVE_REPORT = False


class Sensores(threading.Thread):

    KILL = False
    SET_AUTO = False

    SET_PUMB = False
    SET_SEL_LEFT = False
    SET_SEL_RIGHT = False

    RELAY_PIN_PUMB = 24
    RELAY_PIN_LEFT = 22
    RELAY_PIN_RIGHT = 23

    selenoide_left = OutputDevice(RELAY_PIN_LEFT, active_high=False, initial_value=False)
    selenoide_right = OutputDevice(RELAY_PIN_RIGHT, active_high=False, initial_value=False)
    pumb = OutputDevice(RELAY_PIN_PUMB, active_high=False, initial_value=False)

    def run(self):
        while True:
            if self.SET_AUTO:
                if GPIO_04.value or GPIO_17.value:

                    if GPIO_04.value and GPIO_17.value:
                        self.set_selenoide_right(True)
                        self.set_selenoide_left(True)
                        print('GPIO_04: ' + str(GPIO_04.value) + ' - ' + 'GPIO_17: ' + str(GPIO_17.value))

                    elif GPIO_04.value:
                        self.set_selenoide_right(True)
                        self.set_selenoide_left(False)
                        print('GPIO_04: ' + str(GPIO_04.value) + ' - ' + 'GPIO_17: ' + str(GPIO_17.value))

                    elif GPIO_17.value:
                        self.set_selenoide_right(False)
                        self.set_selenoide_left(True)
                        print('GPIO_04: ' + str(GPIO_04.value) + ' - ' + 'GPIO_17: ' + str(GPIO_17.value))

                    self.set_pump(True)

                else:
                    self.set_selenoide_right(False)
                    self.set_selenoide_left(False)
                    self.set_pump(False)
                    print('GPIO_04: ' + str(GPIO_04.value) + ' - ' + 'GPIO_17: ' + str(GPIO_17.value))
            else:

                if self.SET_PUMB:
                    self.set_pump(True)
                    print(str(self.SET_PUMB))
                else:
                    self.set_pump(False)
                    print('BOMBA: ' + str(self.SET_PUMB))

                if self.SET_SEL_LEFT:
                    self.set_selenoide_left(True)
                    self.set_selenoide_left_on()
                    print('SELENOIDE IZQ: ' + str(self.SET_SEL_LEFT))
                else:
                    self.set_selenoide_left(False)
                    self.set_selenoide_left_off()
                    print('SELENOIDE IZQ: ' + str(self.SET_SEL_LEFT))

                if self.SET_SEL_RIGHT:
                    self.set_selenoide_right(True)
                    self.set_selenoide_right_on()
                    print('SELENOIDE DER: ' + str(self.SET_SEL_RIGHT))
                else:
                    self.set_selenoide_right(False)
                    self.set_selenoide_right_off()
                    print('SELENOIDE DER: ' + str(self.SET_SEL_RIGHT))

            time.sleep(1)

        return

    def kill(self):
        self.KILL = True

    def set_auto_on(self):
        self.SET_AUTO = True

    def set_auto_off(self):
        self.SET_AUTO = False

    def set_pump_on(self):
        self.SET_PUMB = True

    def set_pump_off(self):
        self.SET_PUMB = False

    def set_selenoide_right_on(self):
        self.SET_SEL_RIGHT = True

    def set_selenoide_right_off(self):
        self.SET_SEL_RIGHT = False

    def set_selenoide_left_on(self):
        self.SET_SEL_LEFT = True

    def set_selenoide_left_off(self):
        self.SET_SEL_LEFT = False

    ''' EVENTOS EN SENSORES'''
    def set_pump(self, status):
        if status:
            print("Selenoide PUMB: ON")
            self.pumb.on()
        else:
            print("Selenoide PUMB: OFF")
            self.pumb.off()

    def set_selenoide_right(self, status):
        if status:
            print("Selenoide RIGHT: ON")
            self.selenoide_right.on()
        else:
            print("Selenoide RIGHT: OFF")
            self.selenoide_right.off()

    def set_selenoide_left(self, status):
        if status:
            print("Selenoide LEFT: ON")
            self.selenoide_left.on()
        else:
            print("Selenoide LEFT: OFF")
            self.selenoide_left.off()